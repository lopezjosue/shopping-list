function generateId() {
  const ending = Math.floor(Math.random() * 90) + 10
  return String(Date.now() + ending)
}

export function createItem(description = '', date = new Date().toISOString()) {
  if (!description) return console.error('No description provided')
  const id = generateId()

  const item = {
    description,
    date,
    id,
    done: false,
  }

  return item
}

export function createItemElement(item, removeItem, toggleItem) {
  const itemElement = document.createElement('li')
  const description = document.createElement('span')
  const date = document.createElement('span')
  const checkbox = document.createElement('input')
  const removeButton = document.createElement('button')

  itemElement.classList.add('shoppingItem')
  description.classList.add('description')
  date.classList.add('date')
  checkbox.classList.add('checkbox')
  removeButton.classList.add('removeButton')

  if(item.done) description.classList.add('done')

  checkbox.type = 'checkbox'
  checkbox.checked = item.done
  description.innerHTML = item.description
  date.innerHTML = new Date(item.date).toLocaleDateString('en-us')
  removeButton.type = 'button'
  removeButton.innerHTML = '&#10005;'

  checkbox.addEventListener('change', () => toggleItem(item.id))
  removeButton.addEventListener('click', () => removeItem(item.id))

  itemElement.appendChild(checkbox)
  itemElement.appendChild(description)
  itemElement.appendChild(date)
  itemElement.appendChild(removeButton)

  return itemElement
}
