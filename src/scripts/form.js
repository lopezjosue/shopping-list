import { createItem } from './shoppingItem.js'
import { addItem } from './shoppingList.js'

const form = document.getElementById('itemForm')

form.addEventListener('submit', (event) => {
  event.preventDefault()
  const formData = new FormData(event.target)

  const description = formData.get('description')
  const rawDate = formData.get('dueDate')
  const dueDate = rawDate
    ? new Date(rawDate).toISOString()
    : new Date().toISOString()

  const newItem = createItem(description, dueDate)
  if(newItem) addItem(newItem)
  form.reset()
})
