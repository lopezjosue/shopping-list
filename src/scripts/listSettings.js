import { renderList } from "./shoppingList.js"

const settingsKey = 'settings'

export function initializeSettings () {
  const settings = localStorage.getItem(settingsKey)
  const filter = document.getElementById('filter')
  const sort = document.getElementById('sort')

  let initialSettings
  if (!settings) {
    initialSettings = {
      filter: 'all',
      sort: 'date'
    }
  } else {
    initialSettings = JSON.parse(settings)
  }

  filter.value = initialSettings.filter
  sort.value = initialSettings.sort

  filter.addEventListener('change', (event) => updateSetting('filter', event.target.value))
  sort.addEventListener('change', (event) => updateSetting('sort', event.target.value))

  localStorage.setItem(settingsKey, JSON.stringify(initialSettings))
}

export function getSettings () {
  const rawSettings = localStorage.getItem(settingsKey)
  return JSON.parse(rawSettings)
}

function updateSetting (setting, value) {
  const settings = getSettings()
  const sort = setting === 'sort' ? value : settings.sort
  const filter = setting === 'filter' ? value : settings.filter

  const updatedSettings = {
    filter,
    sort,
  }

  localStorage.setItem(settingsKey, JSON.stringify(updatedSettings))
  renderList()
}