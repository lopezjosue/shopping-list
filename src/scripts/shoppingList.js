import { getSettings, initializeSettings } from './listSettings.js'
import { createItemElement } from './shoppingItem.js'

export const listKey = 'shoppingList'

initializeList()
initializeSettings()
renderList()

function initializeList() {
  const storedList = readList()

  localStorage.setItem(listKey, JSON.stringify(storedList))
}

function renderItems(listElement, shoppingList = []) {
  shoppingList.forEach((item) => {
    const itemElement = createItemElement(item, removeItem, toggleItem)

    listElement.appendChild(itemElement)
  })
}

export function renderList() {
  const settings = getSettings()
  const listElement = document.getElementById('shoppingList')
  listElement.innerHTML = ''
  const storedList = readList()

  const shoppingList = storedList
    .filter((item) => {
      if (settings.filter === 'completed') return item.done
      if (settings.filter === 'uncompleted') return !item.done

      return true
    })
    .sort((itemA, itemB) => {
      if(settings.sort === 'date') return new Date(itemA.date) - new Date(itemB.date)
      if(settings.sort === 'description') return itemA.description.toLowerCase().localeCompare(itemB.description.toLowerCase())
    })

  if(shoppingList.length > 0) {
    renderItems(listElement, shoppingList)
  } else {
    listElement.innerHTML = 'No items here'
  }

}

export function readList() {
  const shoppingList = localStorage.getItem(listKey)
  return JSON.parse(shoppingList) || []
}

export function updateList(newList) {
  if (!newList) return console.error('No list provided')

  const shoppingList = JSON.stringify(newList)
  localStorage.setItem(listKey, shoppingList)
}

export function addItem(item) {
  const shoppingList = readList()
  shoppingList.push(item)
  updateList(shoppingList)
  renderList()
}

export function removeItem(itemId) {
  const shoppingList = readList()
  const filteredList = shoppingList.filter((item) => item.id !== itemId)
  updateList(filteredList)
  renderList()
}

export function toggleItem(itemId) {
  console.log('check')
  const shoppingList = readList()
  const updatedList = shoppingList.map((item) => {
    if (item.id !== itemId) return item

    return {
      description: item.description,
      date: item.date,
      id: item.id,
      done: !item.done,
    }
  })

  updateList(updatedList)
  renderList()
}
