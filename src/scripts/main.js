import './shoppingList.js'
import './form.js'
import { loadHTML } from './loadHtml.js'

loadHTML('header', 'src/elements/header.html')
loadHTML('footer', 'src/elements/footer.html')
